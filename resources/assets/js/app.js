
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
require('./interceptors')

window.Vue = require('vue')

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCyjEg9Nzs_RhPRFW1aTpoudqJNdn-OB3A',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'accommodation',
    require('./components/Accommodation.vue')
)

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
)

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
)

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
)

const app = new Vue({
    el: '#app'
})
