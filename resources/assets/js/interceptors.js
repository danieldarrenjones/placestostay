/**
 * Intercept the incoming responses.
 *
 * Handle any unexpected HTTP errors and pop up modals, etc.
 */
window.axios.interceptors.response.use(function (response) {
    return response
}, function (error) {
    switch (error.response.status) {
        case 401:
            swal({
                title: 'Unauthorized.',
                text: 'Please provide correct login credentials.',
                type: 'info',
                showCloseButton: true,
            })
            break
        case 422:
            swal({
                title: 'Invalid.',
                text: 'Please check your input.',
                type: 'info',
                showCloseButton: true,
            })
            break
        case 429:
            swal({
                title: 'Uh oh.',
                text: 'It looks like you have done that too many times recently, please try that again later',
                type: 'error',
            })
            break
        case 500:
            swal({
                title: 'Uh oh.',
                text: 'An error occured.',
                type: 'error',
            })
            break
    }

    return Promise.reject(error)
})
