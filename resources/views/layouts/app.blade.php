<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Places to Stay' }}</title>

    <!-- CSS  -->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <style media="screen">
        .google-map-canvas,
        .google-map-canvas * {
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

    </style>
</head>
<body>

    <div id="app">

        @include('_partials.nav')


        <div class="container mx-auto px-2">
            {{ $slot }}
        </div>

    </div>

    {{ $javascript or ''}}
    <script src="{{ mix('/js/manifest.js') }}"></script>
    <script src="{{ mix('/js/vendor.js') }}"></script>
    <script src="{{ mix('/js/app.js') }}"></script>

    </body>
    </html>
