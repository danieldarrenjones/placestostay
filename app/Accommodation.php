<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    public $table = 'accommodations';

    public $casts = [
        'latitude' => 'double',
        'longitude' => 'double',
    ];

    public $guarded = [
        
    ];

    public function dates() {
        return $this->hasMany('App\AccommodationDate');
    }
}
