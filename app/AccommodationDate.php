<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationDate extends Model
{
    public $table = 'accommodation_dates';

    public $timestamps = false;

    public $appends = [
        'spaces_left',
    ];

    public function accommodation()
    {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }

    public function getSpacesLeftAttribute()
    {
        return $this->availability - $this->bookings()->count();
    }

    public function bookings() {
        return $this->hasMany('App\AccommodationBooking');
    }
}
