<?php

namespace App\Http\Controllers\Api;

use App\Accommodation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\CreatedAccommodation;
use App\Http\Requests\AccommodationIndexRequest;
use App\Http\Requests\AccommodationCreateRequest;

class AccommodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AccommodationIndexRequest $request)
    {

        $query = Accommodation::query();

        if ($request->input('location')) {
            $query = $query->where('location', 'like', "%{$request->input('location')}%");
        }

        if ($request->input('type')) {
            $query = $query->where('type', 'like', "%{$request->input('type')}%");
        }

        return $query->with(['dates' => function($query) {
            $query->where('availability', '>', 0);
        }])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccommodationCreateRequest $request)
    {
        $accommodation = Accommodation::create([
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'location' => $request->input('location'),
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
            'description' => $request->input('description'),
        ]);

        event(new CreatedAccommodation($accommodation));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function show(Accommodation $accommodation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Accommodation $accommodation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accommodation $accommodation)
    {
        //
    }
}
