<?php

namespace App\Http\Controllers\Api;

use App\AccommodationDate;
use Illuminate\Http\Request;
use App\AccommodationBooking;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccommodationBookingRequest;

class AccommodationBookingController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccommodationBookingRequest $request)
    {
        if (AccommodationDate::find($request->input('accommodation_date_id'))->spaces_left < $request->input('numberOfPeople')) {
            abort(422, 'There are not enough spaces left');
        }

        foreach (range(1, $request->input('numberOfPeople')) as $person) {

            AccommodationBooking::create([
                'accommodation_id' => $request->input('accommodation_id'),
                'accommodation_date_id' => $request->input('accommodation_date_id'),
                'user_id' => 1,
            ]);
        }
    }
}
