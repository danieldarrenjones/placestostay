<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccommodationBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accommodation_id'          => 'required|exists:accommodations,id',
            'accommodation_date_id'     => 'required|exists:accommodation_dates,id',
            'numberOfPeople'            => 'required|min:1|int',
        ];
    }
}
